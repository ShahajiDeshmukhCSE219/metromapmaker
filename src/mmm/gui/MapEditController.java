/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import com.sun.javafx.scene.control.skin.DatePickerContent;
import com.sun.javafx.scene.control.skin.LabeledText;
import djf.AppTemplate;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import mmm.data.MetroLabel;
import mmm.data.MetroLine;
import mmm.data.MetroStation;

import mmm.data.mmmData;
import mmm.data.mmmState;

/**
 * This class is the controller class for the MetroMapMaker application. All the
 * methods are corresponding to the buttons which are present in the UI
 *
 * @author Shahaji
 */
public class MapEditController {

    //This is the app
    AppTemplate app;
    //This is the data maanager for the class
    mmmData dataManager;

    /**
     * This is the constructor of the class
     *
     * @param app this is the app
     */
    public MapEditController(AppTemplate app) {
        this.app = app;
        dataManager = (mmmData) app.getDataComponent();
    }

    /**
     * Processes Add station action
     */
    public void processAddStation() {
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        // CHANGE THE STATE
        dataManager.setState(mmmState.ADD_STATION_MODE);

        // ENABLE/DISABLE THE PROPER BUTTONS
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    public void addStationToLine(String lineName, String stationName) {
        MetroStation ms = (MetroStation) dataManager.findMetroObject(stationName, "station");
        MetroLine ml = (MetroLine) dataManager.findMetroObject(lineName, "line");
        System.out.println("ml--------"+ml.getStationsList().size()+"-----"+ml.getLineName().getText());
        if (ml.getStationsList().size() == 0) {
            System.out.println("im in this shittty loop");
            ml.setEndX(ml.getStartX()+50);ml.setEndY(ml.getStartY());
            ms.setCenterX(ml.getEndX());
            ms.setCenterY(ml.getEndY());
           ml.getConnections().add(ml);
           ml.setLastLine(ml);
        } else {
            System.out.println(ml.getConnections().size()+"======this the size of the connetcs for the lists");
//            Line l=ml.setUpLine(ms);
            ((mmmWorkspace) app.getWorkspaceComponent()).canvas.getChildren().addAll(ml.setUpLine(ms));
            //ml.getConnections().add();
        }
        dataManager.addLineToStation(stationName, ml);
        dataManager.addstationToLine(lineName, ms);
        System.out.println("-------checks if circular or not------bool value:"+ml.isCircular());
    }

    public void removeStationFromLine(String lineName, String stationName) {
        MetroStation ms = (MetroStation) dataManager.findMetroObject(stationName, "station");
        MetroLine ml = (MetroLine) dataManager.findMetroObject(lineName, "line");
        ml.removeMetroStation(ms);
        ms.removeMetroLine(ml);
        for (Node n : ((mmmWorkspace) app.getWorkspaceComponent()).canvas.getChildren()) {
            if (n instanceof Line) {
                System.out.println("----------in remoe stations from line -------for loop");
                Line l = (Line) n;
                System.out.println(n.toString() + "------------line as node" + ms.toString() + "-------------" + n.contains(ms.getCenterX(), ms.getCenterX()));
                if (l.contains(ms.getCenterX(), ms.getCenterY())) {
                    int index = ml.getConnections().indexOf(l);
                    if (index-1>=0 && index+1<ml.getConnections().size()) {
                        System.out.println("-------workded for this---------------");
                        Line newL = new Line();
                        newL.setStrokeWidth(ml.getStrokeWidth());
                        newL.setStroke(ml.getStroke());
                        newL.setStartX(ml.getConnections().get(index).getStartX());
                        newL.setStartY(ml.getConnections().get(index).getStartY());
                        newL.setEndX(ml.getConnections().get(index + 1).getEndX());
                        newL.setEndY(ml.getConnections().get(index + 1).getEndY());
                        ((mmmWorkspace) app.getWorkspaceComponent()).canvas.getChildren().remove(ml.getConnections().get(index + 1));
                        ((mmmWorkspace) app.getWorkspaceComponent()).canvas.getChildren().remove(ml.getConnections().get(index));
                        ml.getConnections().remove(index + 1);
                        ml.getConnections().remove(index - 1);
                        ((mmmWorkspace) app.getWorkspaceComponent()).canvas.getChildren().addAll(newL);
                    }
                    ((mmmWorkspace) app.getWorkspaceComponent()).canvas.getChildren().remove(n);
                    ml.getConnections().remove((Line) n);
                    if (n instanceof MetroLine) {
                        MetroLine ml1 = (MetroLine) l;
                        dataManager.removeMetroObject(ml);
                        ((mmmWorkspace) app.getWorkspaceComponent()).canvas.getChildren().remove(ml1.getLineName());
                    }
                }
            }
        }
    }

    /**
     * Processes Remove Station Action
     */
    public void processRemoveStation() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        // CHANGE THE STATE
        dataManager.setState(mmmState.REMOVE_STATION_MODE);

        // ENABLE/DISABLE THE PROPER BUTTONS
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    /**
     * Processes the add Metro Line action
     */
    public MetroLine processAddLine(String name, Color color1) {
        System.out.println("im here yo");
        return dataManager.startNewMetroLine(color1, name);
    }

    /**
     * Processes the remove metro line action
     */
    public MetroLine processRemoveLine(String lineName) {
        return (MetroLine) dataManager.removeMetroObject(dataManager.findMetroObject(lineName, "line"));
    }

    /**
     * Processes the route functionality of the application
     */
    public void processRoute(MetroStation start, MetroStation end) {
        Text[] text={new Text("bullshit")};
          for(int i=0;i<start.getLinesList().size();i++){
              if(end.getLinesList().contains(start.getLinesList().get(i))){
                  MetroLine ml=(MetroLine)start.getLinesList().get(i);
                  int index1=ml.getStationsList().indexOf(start);
                  int index2=ml.getStationsList().indexOf(end);
                  
                  if(index1>index2){
                      System.out.println("in first if ");
                    text=new Text[index1-index2];
                    System.out.println("------------length"+text.length);
                      int a=0;
                    for(int j=index2; j<=index1; j++,a++){
                        MetroStation ms=(MetroStation)ml.getStationsList().get(j);
                        
                        text[a]= new Text(ms.getStationName().getText());
                        System.out.println("this si themsssss"+ms.toString());
                                            }
                  }else{
                      
                      text=new Text[index2-index1];
                      System.out.println("------------length"+text.length);
                      int a=0;
                    for(int j=index2; j<=index1; j++,a++){
                        System.out.println("in the foor looop ");
                        MetroStation ms=(MetroStation)ml.getStationsList().get(j);
                        System.out.println("this si themsssss"+ms.toString());
                        text[a]= new Text(ms.getStationName().getText());
                        System.out.println(text[a].getText());
                    }
                  }
              }
          }
 //         Stage s=new Stage();
//          VBox v=new VBox(text);
  //        s.setScene(new Scene(v));
 //         s.setAlwaysOnTop(true);
  //        s.show();
    }

    /**
     * This processes the set background action of the application
     */
    public void processSetBackground() {

    }

    /**
     * Processes add image action
     */
    public void processAddImage() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        // CHANGE THE STATE
        dataManager.setState(mmmState.ADD_METRO_IMAGE);
        System.out.println(dataManager.getState().toString()+"--------is it correct");
        // ENABLE/DISABLE THE PROPER BUTTONS
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    /**
     * Processes the add label action
     */
    public void processAddLabel() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        // CHANGE THE STATE
        
        dataManager.setState(mmmState.ADD_METRO_LABEL);
        System.out.println(dataManager.getState().toString()+"--------is it correct");
        // ENABLE/DISABLE THE PROPER BUTTONS
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
//        DataDialogSingleton dds=new DataDialogSingleton("Text");
//        MetroLabel mLabel=dataManager.startNewMetroLabel(dds.showdialog());
//        dataManager.setState(mmmState.ADD_METRO_LABEL);
    }

    /**
     * Processes the remove element action
     */
    public void processRemoveElement() {

    }

    /**
     * Processes the embolden text action
     */
    public void processBoldenText() {

    }

    /**
     * Processes the italicize text action
     */
    public void processItalcizeText() {

    }

    /**
     * Processes the set font size action for the text or metro labels
     */
    public void processSetFontSize() {

    }

    /**
     * Processes the set font family action for the text or metro labels
     */
    public void processSetFontFamily() {

    }

    /**
     * Processes the viewports zoom in action
     */
    public void processZoomIn() {

    }

    /**
     * Processes the viewports zoom out action
     */
    public void processZoomOut() {

    }

    /**
     * Processes the viewports panning abilities
     */
    public void processExpandView() {

    }

    /**
     * Processes the viewports panning abilities
     */
    public void processContractView() {

    }

    /**
     * Process the rotate action on the text's alignment.
     */
    public void processRotate(String stationName) {
        MetroStation ms=(MetroStation)dataManager.findMetroObject(stationName, "station");
        ms.getStationName().setRotate(ms.getStationName().getRotate()+90);
    }

    /**
     * Processes list all stations action , by list all the station on a given
     * line
     */
    public void processListAllStations(String lineName) {
        MetroLine ml = (MetroLine) dataManager.findMetroObject(lineName, "line");
        Stage s = new Stage();
        s.setTitle("List Of All Stations in " + ml.getLineName().getText());
        VBox p = new VBox();
        for (int i = 0; i < ml.getStationsList().size(); i++) {
            MetroStation ms = (MetroStation) ml.getStationsList().get(i);
            Text t = new Text(ms.getStationName().getText());
            p.getChildren().add(t);

        }
        s.setScene(new Scene(p));
        s.setAlwaysOnTop(true);
        s.show();
    }

    /**
     * Processes the snap action
     */
    public void processSnap() {

    }

    public void processLineEndDrag() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        // CHANGE THE STATE
        dataManager.setState(mmmState.DRAGGING_METRO_LINE_END);

        // ENABLE/DISABLE THE PROPER BUTTONS
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    public void processSnapshot() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
        File file = new File("Map.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}
