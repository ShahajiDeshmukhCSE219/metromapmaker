/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author Shahaji
 */
public class DataDialogSingleton extends Stage {
    Button okButton;
    Button cancelButton;
    Label labelName;
    TextField nameField;
    Label Color;
    ColorPicker choooser;
    String name;
    boolean action;

    public DataDialogSingleton() {
        okButton=new Button("Apply");
        cancelButton=new Button("Cancel");
        labelName=new Label("Enter Metro Line Name");
        nameField=new TextField("Metro Line");
        choooser=new ColorPicker();
        FlowPane pane=new FlowPane(labelName, nameField, choooser,okButton,cancelButton);
        setScene(new Scene(pane));
    }
    
    public DataDialogSingleton(String type){
        if(type.equals("")){
        okButton=new Button("Apply");
        cancelButton=new Button("Cancel");
        labelName=new Label("Enter Metro Station Name");
        nameField=new TextField("Metro Station");
        FlowPane pane=new FlowPane(labelName, nameField,okButton,cancelButton);
        setScene(new Scene(pane));
        }else if(type.equals("Text")){
            okButton=new Button("Apply");
        cancelButton=new Button("Cancel");
        labelName=new Label("Enter the text");
        nameField=new TextField("Text");
        FlowPane pane=new FlowPane(labelName, nameField,okButton,cancelButton);
        setScene(new Scene(pane));
        }else{
        okButton=new Button("Yes");
        cancelButton=new Button("No");
        nameField=new TextField("Are you sure you want to delete this?");
        FlowPane pane=new FlowPane(labelName,okButton,cancelButton);
        setScene(new Scene(pane));
        }
    }
    
    public Object[] showDialog(){
    
    Object[] settings=new Object[2];
    settings[0]="blue";
    settings[1]=choooser.getValue();
    okButton.setOnAction(e->{
    settings[0]=nameField.getText();
    settings[1]=choooser.getValue();
    close();
    });
    cancelButton.setOnAction(e->{
    close();
    });
    showAndWait();
    return settings;
    }
    
    public String showdialog(){
        okButton.setOnAction(e->{
            name=nameField.getText();
            close();
        });
        cancelButton.setOnAction(e->{
            name="";
            close();
        });
        showAndWait();
        return name;
    }
    
    public boolean showYesNoDialog(){
        okButton.setOnAction(e->{
            action=true;
            close();
        });
        cancelButton.setOnAction(e->{
            action=false;
            close();
        });
        showAndWait();
        return action;
    }
}
