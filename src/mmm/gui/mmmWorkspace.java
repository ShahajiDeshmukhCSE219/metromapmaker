package mmm.gui;

import com.sun.javafx.charts.ChartLayoutAnimator;
import com.sun.javafx.collections.ElementObservableListDecorator;
import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import static mmm.mmmLanguageProperty.ELLIPSE_ICON;
import static mmm.mmmLanguageProperty.ELLIPSE_TOOLTIP;
import static mmm.mmmLanguageProperty.MOVE_TO_BACK_ICON;
import static mmm.mmmLanguageProperty.MOVE_TO_BACK_TOOLTIP;
import static mmm.mmmLanguageProperty.MOVE_TO_FRONT_ICON;
import static mmm.mmmLanguageProperty.MOVE_TO_FRONT_TOOLTIP;
import static mmm.mmmLanguageProperty.RECTANGLE_ICON;
import static mmm.mmmLanguageProperty.RECTANGLE_TOOLTIP;
import static mmm.mmmLanguageProperty.REMOVE_ICON;
import static mmm.mmmLanguageProperty.REMOVE_TOOLTIP;
import static mmm.mmmLanguageProperty.SELECTION_TOOL_ICON;
import static mmm.mmmLanguageProperty.SELECTION_TOOL_TOOLTIP;
import static mmm.mmmLanguageProperty.SNAPSHOT_ICON;
import static mmm.mmmLanguageProperty.SNAPSHOT_TOOLTIP;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.controller.AppFileController;
import static mmm.css.mmmStyle.*;
import mmm.data.DraggableImage;
import mmm.mmmLanguageProperty;
import static mmm.mmmLanguageProperty.ABOUT_ICON;
import static mmm.mmmLanguageProperty.ABOUT_TOOLTIP;
import static mmm.mmmLanguageProperty.CHANGELANGUAGE_ICON;
import static mmm.mmmLanguageProperty.CHANGELANGUAGE_TOOLTIP;
import static mmm.mmmLanguageProperty.CONTRACT_ICON;
import static mmm.mmmLanguageProperty.CONTRACT_TOOLTIP;
import static mmm.mmmLanguageProperty.COPY_ICON;
import static mmm.mmmLanguageProperty.COPY_TOOLTIP;
import static mmm.mmmLanguageProperty.CUT_ICON;
import static mmm.mmmLanguageProperty.CUT_TOOLTIP;
import static mmm.mmmLanguageProperty.EXPAND_ICON;
import static mmm.mmmLanguageProperty.EXPAND_TOOLTIP;
import static mmm.mmmLanguageProperty.LIST_ICON;
import static mmm.mmmLanguageProperty.LIST_TOOLTIP;
import static mmm.mmmLanguageProperty.PASTE_ICON;
import static mmm.mmmLanguageProperty.PASTE_TOOLTIP;
import static mmm.mmmLanguageProperty.ROTATE_ICON;
import static mmm.mmmLanguageProperty.ROTATE_TOOLTIP;
import static mmm.mmmLanguageProperty.ROUTE_ICON;
import static mmm.mmmLanguageProperty.ROUTE_TOOLTIP;
import static mmm.mmmLanguageProperty.ZOOM_IN_ICON;
import static mmm.mmmLanguageProperty.ZOOM_IN_TOOLTIP;
import static mmm.mmmLanguageProperty.ZOOM_OUT_ICON;
import static mmm.mmmLanguageProperty.ZOOM_OUT_TOOLTIP;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ModifiableObservableListBase;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import mmm.data.AddImageAndTextController;
import mmm.data.MetroLabel;
import mmm.data.MetroLine;
import mmm.data.MetroStation;
import mmm.data.mmmData;
import mmm.data.mmmState;
import mmm.file.mmmFile;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class mmmWorkspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    // HAS ALL THE CONTROLS FOR EDITING
    VBox editToolbar;

    //Language variables
    public static String CURRENTFILE;
    public static String SPANISH = "app_properties_EN.xml";
    public static String ENGLISH = "app_properties_EN.xml";

    //Cut/Copy/Paste Clipboard
    Shape clipBoard = null;

    // FIRST ROW
    VBox row1VBox;
    HBox hbox11;
    Label metroLine;
    ComboBox<String> metroLines;
    Button metroLinesColor;
    Label metroLineColorLabel;
    HBox hbox12;
    Button addLine;
    Button removeLine;
    Button addStation;
    Button removeStationFromLine;
    Button listStations;
    Slider metroLinethickness;

    Button changeLanguage;
    Button copyButton;
    Button cutButton;
    Button pasteButton;
    Button aboutButton;
    Button redoButton;
    Button undoButton;
    // SECOND ROW
    VBox row2VBox;
    HBox hbox21;
    Label metroStationsLabel;
    ComboBox<String> stationsCombobox;
    ColorPicker stationColorPicker;
    Label stationColor;
    HBox hbox22;
    Button addStations;
    Button removeStations;
    Button snap;
    Button moveLabel;
    Button rotate;
    Slider stationSlider;

    // THIRD ROW
    HBox row3HBox;
    VBox vbox31;
    ComboBox currentStation;
    ComboBox destination;
    Button caculateRoute;

    // FORTH ROW
    VBox row4VBox;
    HBox row41;
    Label decoreLabel;
    ColorPicker decorColorPicker;
    HBox hbox42;
    Button backgroundImage;
    Button addImage;
    Button addLabel;
    Button removeElemet;

    // FIFTH ROW
    VBox row5VBox;
    HBox hbox51;
    Label fonttLabel;
    ColorPicker fontColorPicker;
    HBox hbox52;
    Button bolden;
    Button italicize;
    ComboBox fontSize;
    ComboBox fontFamily;

    // SIXTH ROW
    VBox row6VBox;
    HBox hbox61;
    Label navigationLabel;
    CheckBox snapButton;
    HBox hbox62;
    Button zoomIn;
    Button zoomOut;
    Button contractView;
    Button expandView;

    JFileChooser jFc = new JFileChooser();

    // THIS IS WHERE WE'LL RENDER OUR DRAWING, NOTE THAT WE
    // CALL THIS A CANVAS, BUT IT'S REALLY JUST A Pane
    Pane canvas;

    // HERE ARE THE CONTROLLERS
    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;

    // FOR DISPLAYING DEBUG STUFF
    Text debugText;
    //Keeps check of which language should be displayed. 
    boolean languageChanged = false;
    String defaultLanguageFile = "app_properties_EN.xml";
    int boldCounter=0;
    int italicCounter=0;
    MapEditController mapEditController;
    MapCanvasController mapCanvasController;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public mmmWorkspace(AppTemplate initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();

    }

    /**
     * Note that this is for displaying text during development.
     */
    public void setDebugText(String text) {
        debugText.setText(text);
    }

    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
    public Pane getCanvas() {
        return canvas;
    }

    // HELPER SETUP METHOD
    private void initLayout() {
        // THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
        System.setProperty("glass.accessible.force", "false");
        editToolbar = new VBox();
        // ROW 1
        row1VBox = new VBox();
        hbox11 = new HBox();
        hbox11.setStyle("-fx-spacing:15;");
        hbox12 = new HBox();
        hbox12.setStyle("-fx-spacing:5;");
        row1VBox.getChildren().addAll(hbox11, hbox12);
        metroLine = new Label("Metro Line");
        metroLines = new ComboBox<String>();
        metroLines.setPromptText("MetroLine");
        metroLinesColor = new Button();
        metroLineColorLabel = new Label();
        //metroLineColorLabel.setGraphic();
        hbox11.getChildren().addAll(metroLine, metroLines, metroLinesColor, metroLineColorLabel);
        addLine = new Button("+");
        removeLine = new Button("-");
        addStation = new Button("Add Station");
        removeStationFromLine = new Button("Remove Station");
        metroLinethickness = new Slider();
        hbox12.getChildren().addAll(addLine, removeLine, addStation, removeStationFromLine);
        listStations = gui.initChildButton(hbox12, LIST_ICON.toString(), LIST_TOOLTIP.toString(), false);
        row1VBox.getChildren().add(metroLinethickness);
        cutButton = gui.initChildButton(gui.getFileToolbar(), CUT_ICON.toString(), CUT_TOOLTIP.toString(), false);
        copyButton = gui.initChildButton(gui.getFileToolbar(), COPY_ICON.toString(), COPY_TOOLTIP.toString(), false);
        //pasteButton = gui.initChildButton(gui.getFileToolbar(), PASTE_ICON.toString(), PASTE_TOOLTIP.toString(), true);
        //changeLanguage = gui.initChildButton(gui.getFileToolbar(), CHANGELANGUAGE_ICON.toString(), CHANGELANGUAGE_TOOLTIP.toString(), false);
        aboutButton = gui.initChildButton(gui.getFileToolbar(), ABOUT_ICON.toString(), ABOUT_TOOLTIP.toString(), false);
        undoButton = gui.initChildButton(gui.getFileToolbar(), mmmLanguageProperty.UNDO_ICON.toString(), mmmLanguageProperty.UNDO_TOOLTIP.toString(), false);
        redoButton = gui.initChildButton(gui.getFileToolbar(), mmmLanguageProperty.REDO_ICON.toString(), mmmLanguageProperty.REDO_TOOLTIP.toString(), false);
        // ROW 2
        row2VBox = new VBox();
        hbox21 = new HBox();
        hbox22 = new HBox();
        hbox22.setStyle("-fx-spacing:15;");
        hbox21.setStyle("-fx-spacing:15;");
        row2VBox.getChildren().addAll(hbox21, hbox22);
        metroStationsLabel = new Label("Metro Stations");
        stationsCombobox = new ComboBox<String>();
        stationsCombobox.setPromptText("Metro Station");
        stationColorPicker = new ColorPicker();
        hbox21.getChildren().addAll(metroStationsLabel, stationsCombobox, stationColorPicker);
        addStations = new Button("+");
        removeStations = new Button("-");
        snap = new Button("Snap");
        moveLabel = new Button("Move Label");
        hbox22.getChildren().addAll(addStations, removeStations, snap, moveLabel);
        rotate = gui.initChildButton(hbox22, ROTATE_ICON.toString(), ROTATE_TOOLTIP.toString(), false);
        stationSlider = new Slider();
        row2VBox.getChildren().add(stationSlider);

        // ROW 3
        row3HBox = new HBox();
        row3HBox.setStyle("-fx-spacing:20;");
        vbox31 = new VBox();
        currentStation = new ComboBox<String>();
        destination = new ComboBox<String>();
        currentStation.setPrefWidth(260);
        destination.setPrefWidth(260);
        currentStation.setPromptText("Station 1");
        destination.setPromptText("Station 2");
        vbox31.getChildren().addAll(currentStation, destination);
        row3HBox.getChildren().addAll(vbox31);
        caculateRoute = gui.initChildButton(row3HBox, ROUTE_ICON.toString(), ROUTE_TOOLTIP.toString(), languageChanged);

        // ROW 4
        row4VBox = new VBox();
        row41 = new HBox();
        hbox42 = new HBox();
        row4VBox.getChildren().addAll(row41, hbox42);
        decoreLabel = new Label("Decore");
        decorColorPicker = new ColorPicker();
        row41.setStyle("-fx-spacing:180;");
        row41.getChildren().addAll(decoreLabel, decorColorPicker);
        backgroundImage = new Button("Set Background Image");
        addImage = new Button("Add Image");
        addLabel = new Button("Add Label");
        removeElemet = new Button("Remove Element");
        hbox42.getChildren().addAll(addImage, addLabel, removeElemet);
        hbox42.setStyle("-fx-spacing:37");
        row4VBox.getChildren().add(backgroundImage);

        // ROW 5
        row5VBox = new VBox();
        hbox51 = new HBox();
        hbox51.setStyle("-fx-spacing: 195");
        hbox52 = new HBox();
        hbox52.setStyle("-fx-spacing:29");
        row5VBox.getChildren().addAll(hbox51, hbox52);
        fonttLabel = new Label("Font");
        fontColorPicker = new ColorPicker();
        hbox51.getChildren().addAll(fonttLabel, fontColorPicker);
        bolden = new Button("B");
        bolden.setStyle("-fx-font-weight: bold;");
        italicize = new Button("I");
        italicize.setStyle("-fx-font-style:italic;-fx-font-weight:bold;");
        fontSize = new ComboBox<Double>();
        fontSize.setPrefWidth(90);
        fontSize.getItems().addAll(
        new Double(10),new Double(12),new Double(14),
        new Double(16),new Double(18),new Double(20),
        new Double(22),new Double(24)
        );
        fontSize.setValue(new Double(16));
        fontFamily = new ComboBox<String>();
        fontFamily.setPrefWidth(110);
        fontFamily.getItems().addAll(
               new String("Serif"),
                new String("Tahoma"),
                new String("Comic Sans"),
                new String("Verdana"),
                new String("Arial")
        );
        fontSize.setPromptText("font size");
        fontFamily.setPromptText("font family");
        hbox52.getChildren().addAll(bolden, italicize, fontSize, fontFamily);

        // ROW 6
        row6VBox = new VBox();
        hbox61 = new HBox();
        hbox61.setStyle("-fx-spacing:160");
        hbox62 = new HBox();
        hbox62.setStyle("-fx-spacing:16");
        row6VBox.getChildren().addAll(hbox61, hbox62);
        navigationLabel = new Label("Navigation");
        snapButton = new CheckBox("Show Grid");
        hbox61.getChildren().addAll(navigationLabel, snapButton);
        zoomIn = gui.initChildButton(hbox62, ZOOM_IN_ICON.toString(), ZOOM_IN_TOOLTIP.toString(), false);
        zoomOut = gui.initChildButton(hbox62, ZOOM_OUT_ICON.toString(), ZOOM_OUT_TOOLTIP.toString(), false);
        contractView = gui.initChildButton(hbox62, EXPAND_ICON.toString(), EXPAND_TOOLTIP.toString(), false);
        expandView = gui.initChildButton(hbox62, CONTRACT_ICON.toString(), CONTRACT_TOOLTIP.toString(), false);

        editToolbar.getChildren().add(row1VBox);
        editToolbar.getChildren().add(row2VBox);
        editToolbar.getChildren().add(row3HBox);
        editToolbar.getChildren().add(row4VBox);
        editToolbar.getChildren().add(row5VBox);
        editToolbar.getChildren().add(row6VBox);
        // WE'LL RENDER OUR STUFF HERE IN THE CANVAS
        canvas = new Pane();
        debugText = new Text("whats wrong");
        canvas.getChildren().add(debugText);
        debugText.setX(100);
        debugText.setY(100);
        System.out.println(canvas.getChildren().toString());
        // AND MAKE SURE THE DATA MANAGER IS IN SYNC WITH THE PANE
        mmmData data = (mmmData) app.getDataComponent();
//        data.setShapes(canvas.getChildren());
        data.setShapes(FXCollections.observableArrayList());

        // AND NOW SETUP THE WORKSPACE
        workspace = new BorderPane();
        ((BorderPane) workspace).setLeft(editToolbar);
        ((BorderPane) workspace).setCenter(canvas);

    }

    public ComboBox<String> getMetroLines() {
        return metroLines;
    }

    public ComboBox<String> getStationsCombobox() {
        return stationsCombobox;
    }

    // HELPER SETUP METHOD
    private void initControllers() {
        mapEditController = new MapEditController(app);
        mapCanvasController = new MapCanvasController(app);
        mmmData dataManager = (mmmData) app.getDataComponent();
        // MAKE THE EDIT CONTROLLER
        
        snap.setOnAction(e->{
            File f=new File("");
            String str=f.getAbsolutePath();
            str=str+"\\Map.json";
            try{
            ((mmmFile)app.getFileComponent()).exportData(dataManager, str);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        });
        
        
        
        aboutButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String filePath = new File("").getAbsolutePath();
                System.out.println(filePath);
                filePath += ("\\aboutApp.png");
                System.out.println(filePath);
                ImageIcon icon = new ImageIcon(filePath);
                //UIManager.put("OptionPane.minimumSize",new Dimension(500,500)); 
                JOptionPane.showMessageDialog(
                        null,
                        "MetroMapMaker App\nDeveloped By:\nShahaji Deshmukh\nDeveloped in 2017",
                        "About", JOptionPane.INFORMATION_MESSAGE,
                        icon);
            }
        });

        addLine.setOnAction(e -> {
            DataDialogSingleton lds = new DataDialogSingleton();
            Object[] settings = lds.showDialog();
            Color hexCode = (Color) settings[1];
            metroLinesColor.setText(hexCode.toString().substring(2));
            String code = String.format("#%02X%02X%02X",
                    (int) (hexCode.getRed() * 255),
                    (int) (hexCode.getGreen() * 255),
                    (int) (hexCode.getBlue() * 255));
            metroLinesColor.setStyle("-fx-background-color:" + code + ";");
            Color c = new Color(hexCode.getRed(), hexCode.getGreen(), hexCode.getBlue(), hexCode.getOpacity());
            MetroLine currentLine = mapEditController.processAddLine((String) settings[0], c);
            metroLines.getItems().add(currentLine.getLineName().getText());
            metroLines.setValue(currentLine.getLineName().getText());
            
            //currentLine.setStartX(100);currentLine.setStartY(100);currentLine.setEndX(200);currentLine.setEndY(100);
            //canvas.getChildren().add(currentLine);
            
            dataManager.setState(mmmState.ADD_LINE_MODE);
            dataManager.setCurrentLine(currentLine);
            System.out.println(((mmmData) app.getDataComponent()).getMetroMapShapes().toString());
        });
        
        caculateRoute.setOnAction(e->{
            MetroStation ms1=(MetroStation)dataManager.findMetroObject((String)currentStation.getValue(), "station");
            MetroStation ms2=(MetroStation)dataManager.findMetroObject((String)destination.getValue(), "station");
            mapEditController.processRoute(ms1,  ms2);
        });
        
        metroLinesColor.setOnAction(e -> {
            DataDialogSingleton lds = new DataDialogSingleton();
            lds.showDialog();
        });
        
        decorColorPicker.setOnAction(e->{
            Color hexCode=decorColorPicker.getValue();
            dataManager.setBackgroundColor(hexCode);
        });
        
        cutButton.setOnAction(e->{
            
            System.out.println("fghngdkjgfngdkjfngdfgdfg");
            //((mmmFile)app.getFileComponent()).exportData(dataManager, );
        });
        
        backgroundImage.setOnAction(e->{
            String height=Double.toString(canvas.getHeight());
            String width=Double.toString(canvas.getWidth());
            AddImageAndTextController ad=new AddImageAndTextController(app);
            //String str=ad.promptForImage1();
            //str="file://"+str;
            //str.replace('\\','/');
            //System.out.println("-----------"+str);
            //width="500";
//            dataManager.setBackgroundImage(str);
            //canvas.setStyle("-fx-background-image: url(\""+str+"\");"
             //       + "-fx-background-size : 741px 1800px;");
            canvas.setStyle("-fx-background-image: url(\"https://www.newton.ac.uk/files/covers/968361.jpg\");"
                    + "-fx-background-size : 741px 1800px;");
        });
        
        
        removeLine.setOnAction(e -> {
            System.out.println((String) metroLines.getValue());
            MetroLine ml = mapEditController.processRemoveLine((String) metroLines.getValue());
            canvas.getChildren().remove(ml.getLineName());
            canvas.getChildren().removeAll(ml.getConnections());
            canvas.getChildren().remove(ml);
            boolean v = metroLines.getItems().remove(metroLines.getValue());
        });
        addStation.setOnAction(e -> {
            mapEditController.addStationToLine(metroLines.getValue(), stationsCombobox.getValue());
        });
        
        removeStationFromLine.setOnAction(e->{
            System.out.println("------------in remove statiosnfrom line event------------------");
            mapEditController.removeStationFromLine(metroLines.getValue(), stationsCombobox.getValue());
        });
        
        addStations.setOnAction(e -> {
            mapEditController.processAddStation();
        });

        listStations.setOnAction(e->{
        mapEditController.processListAllStations(metroLines.getValue());
        });
        
        metroLinethickness.setOnMouseDragged(e->{
            MetroLine ml=(MetroLine)dataManager.findMetroObject(metroLines.getValue(), "line");
            ml.setStrokeWidth(metroLinethickness.getValue());
            for(int i=0;i<ml.getConnections().size();i++){
                ml.getConnections().get(i).setStrokeWidth(metroLinethickness.getValue());
            }
        });
        
        stationSlider.setOnMouseDragged(e->{
            MetroStation ms=(MetroStation)dataManager.findMetroObject(stationsCombobox.getValue(), "station");
            ms.setRadius(stationSlider.getValue());
        });
        
        moveLabel.setOnAction(e->{
            MetroStation ms=(MetroStation)dataManager.findMetroObject(stationsCombobox.getValue(), "station");
            ms.moveLabel();
        });
        
        addImage.setOnAction(e->{
            mapEditController.processAddImage();
        });
        
        addLabel.setOnAction(e->{
            mapEditController.processAddLabel();
        });
        
        rotate.setOnAction(e->{
            mapEditController.processRotate(stationsCombobox.getValue());
        });
        
        bolden.setOnAction(e->{
            if(boldCounter%2==0){
            if(dataManager.getCurrentShape() instanceof MetroLine){
                MetroLine ml=(MetroLine)dataManager.getCurrentShape();
                ml.getLineName().setFont(Font.font(ml.getLineName().getFont().getFamily(), FontWeight.EXTRA_BOLD, 
                        FontPosture.REGULAR, ml.getLineName().getFont().getSize()));
                //ml.getLineName2().setFont(Font.font(ml.getLineName2().getFont().getFamily(), FontWeight.EXTRA_BOLD, 
                  //      FontPosture.REGULAR, ml.getLineName2().getFont().getSize()));
            }else if(dataManager.getCurrentShape() instanceof MetroStation){
                MetroStation ms=(MetroStation)dataManager.getCurrentShape();
                ms.getStationName().setFont(Font.font(ms.getStationName().getFont().getFamily(), FontWeight.EXTRA_BOLD, 
                        FontPosture.REGULAR, ms.getStationName().getFont().getSize()));
            }else if(dataManager.getCurrentShape() instanceof MetroLabel){
                MetroLabel mLabel=(MetroLabel)dataManager.getCurrentShape();
                mLabel.setFont(Font.font(mLabel.getFont().getFamily(), FontWeight.EXTRA_BOLD, 
                        FontPosture.REGULAR, mLabel.getFont().getSize()));
            }
            }else{
                if(dataManager.getCurrentShape() instanceof MetroLine){
                MetroLine ml=(MetroLine)dataManager.getCurrentShape();
                ml.getLineName().setFont(Font.font(ml.getLineName().getFont().getFamily(), FontWeight.NORMAL, 
                        FontPosture.REGULAR, ml.getLineName().getFont().getSize()));
                //ml.getLineName2().setFont(Font.font(ml.getLineName2().getFont().getFamily(), FontWeight.NORMAL, 
                  //      FontPosture.REGULAR, ml.getLineName2().getFont().getSize()));
            }else if(dataManager.getCurrentShape() instanceof MetroStation){
                MetroStation ms=(MetroStation)dataManager.getCurrentShape();
                ms.getStationName().setFont(Font.font(ms.getStationName().getFont().getFamily(), FontWeight.NORMAL, 
                        FontPosture.REGULAR, ms.getStationName().getFont().getSize()));
            }else if(dataManager.getCurrentShape() instanceof MetroLabel){
                MetroLabel mLabel=(MetroLabel)dataManager.getCurrentShape();
                mLabel.setFont(Font.font(mLabel.getFont().getFamily(), FontWeight.NORMAL, 
                        FontPosture.REGULAR, mLabel.getFont().getSize()));
            }
            }
            boldCounter++;
        });
        
        italicize.setOnAction(e->{
            if(italicCounter%2==0){
            if(dataManager.getCurrentShape() instanceof MetroLine){
                MetroLine ml=(MetroLine)dataManager.getCurrentShape();
                ml.getLineName().setFont(Font.font(ml.getLineName().getFont().getFamily(), FontWeight.NORMAL, 
                        FontPosture.ITALIC, ml.getLineName().getFont().getSize()));
                //ml.getLineName2().setFont(Font.font(ml.getLineName2().getFont().getFamily(), FontWeight.NORMAL, 
                  //      FontPosture.REGULAR, ml.getLineName2().getFont().getSize()));
            }else if(dataManager.getCurrentShape() instanceof MetroStation){
                MetroStation ms=(MetroStation)dataManager.getCurrentShape();
                ms.getStationName().setFont(Font.font(ms.getStationName().getFont().getFamily(), FontWeight.NORMAL, 
                        FontPosture.ITALIC, ms.getStationName().getFont().getSize()));
            }else if(dataManager.getCurrentShape() instanceof MetroLabel){
                MetroLabel mLabel=(MetroLabel)dataManager.getCurrentShape();
                mLabel.setPosture(FontPosture.ITALIC);
                mLabel.setFont(Font.font(mLabel.getFont().getFamily(), mLabel.getWeight(), 
                        mLabel.getPosture(), mLabel.getFontSize()));
            }}else{
            if(dataManager.getCurrentShape() instanceof MetroLine){
                MetroLine ml=(MetroLine)dataManager.getCurrentShape();
                ml.getLineName().setFont(Font.font(ml.getLineName().getFont().getFamily(), FontWeight.NORMAL, 
                        FontPosture.REGULAR, ml.getLineName().getFont().getSize()));
                //ml.getLineName2().setFont(Font.font(ml.getLineName2().getFont().getFamily(), FontWeight.NORMAL, 
                  //      FontPosture.REGULAR, ml.getLineName2().getFont().getSize()));
            }else if(dataManager.getCurrentShape() instanceof MetroStation){
                MetroStation ms=(MetroStation)dataManager.getCurrentShape();
                ms.getStationName().setFont(Font.font(ms.getStationName().getFont().getFamily(), FontWeight.NORMAL, 
                        FontPosture.REGULAR, ms.getStationName().getFont().getSize()));
            }else if(dataManager.getCurrentShape() instanceof MetroLabel){
                MetroLabel mLabel=(MetroLabel)dataManager.getCurrentShape();
                mLabel.setPosture(FontPosture.REGULAR);
                mLabel.setFont(Font.font(mLabel.getFont().getFamily(), mLabel.getWeight(), 
                        mLabel.getPosture(), mLabel.getFontSize()));
            }
           } italicCounter++;
        });
        
        fontFamily.setOnAction((Event e)->{
            if(dataManager.getCurrentShape() instanceof MetroLine){
                MetroLine ml=(MetroLine)dataManager.getCurrentShape();
                ml.getLineName().setFont(Font.font((String)fontFamily.getValue(), FontPosture.REGULAR, ml.getLineName().getFont().getSize()));
            }else if(dataManager.getCurrentShape() instanceof MetroStation){
                MetroStation ms=(MetroStation)dataManager.getCurrentShape();
                ms.getStationName().setFont(Font.font((String)fontFamily.getValue()));
            }else if(dataManager.getCurrentShape() instanceof MetroLabel){
                MetroLabel mLabel=(MetroLabel)dataManager.getCurrentShape();
                mLabel.setFont(Font.font((String)fontFamily.getValue(),mLabel.getWeight(), mLabel.getPosture(), mLabel.getFontSize()));
            }
        });
        removeElemet.setOnAction(e->{
            Shape s=dataManager.getCurrentShape();
            canvas.getChildren().remove(s);
            dataManager.removeMetroObject(s);
        });
        
        fontSize.setOnAction(e->{
            if(dataManager.getCurrentShape() instanceof MetroLine){
                MetroLine ml=(MetroLine)dataManager.getCurrentShape();
                ml.getLineName().setFont(Font.font((Double)fontSize.getValue()));
            }else if(dataManager.getCurrentShape() instanceof MetroStation){
                MetroStation ms=(MetroStation)dataManager.getCurrentShape();
                ms.getStationName().setFont(Font.font((Double)fontSize.getValue()));
            }else if(dataManager.getCurrentShape() instanceof MetroLabel){
                MetroLabel mLabel=(MetroLabel)dataManager.getCurrentShape();
                mLabel.setFont(Font.font((Double)fontSize.getValue()));
            }
        });
        
        canvas.setOnMouseClicked(e -> {
            mapCanvasController.processMapEditorMousePress(e.getX(), e.getY());
        });
        
        
        canvas.setOnMouseDragged(e -> {
            dataManager.setState(mmmState.DRAGGING_METRO_SHAPE);
            if (getTopShapeCanvas(e.getX(), e.getY()) instanceof MetroLabel) {
                System.out.println("please be here this if for metro label");
                //mapEditController.processLineEndDrag();
                mapCanvasController.processMapEditorMouseDragged(e.getX(), e.getY());
            }else if(getTopShapeCanvas(e.getX(), e.getY()) instanceof Text){
                mapEditController.processLineEndDrag();
            }
            mapCanvasController.processMapEditorMouseDragged(e.getX(), e.getY());
        });

        removeStations.setOnAction(e -> {
            mapEditController.processRemoveStation();
        });
        mmmData md = (mmmData) app.getDataComponent();
        canvas.getChildren().setAll(md.getMetroMapShapes());

        cutButton.setOnAction(e -> {
            mapEditController.processSnapshot();
        });
        
        fontColorPicker.setOnAction(e->{
            Color c=fontColorPicker.getValue();
            if((dataManager.getCurrentShape() instanceof MetroLabel)){
            dataManager.getCurrentShape().setStroke(c);
            }else if(dataManager.getCurrentShape() instanceof MetroLine){
                ((MetroLine)dataManager.getCurrentShape()).getLineName().setStroke(c);
            }else if(dataManager.getCurrentShape() instanceof MetroStation){
                ((MetroStation)dataManager.getCurrentShape()).getStationName().setStroke(c);
            }
        });
        app.getGUI().updateToolbarControls(true);
    }

    public ComboBox getCurrentStation() {
        return currentStation;
    }

    public ComboBox getDestination() {
        return destination;
    }

    // HELPER METHOD
    public void loadSelectedShapeSettings(Shape shape) {
        if (shape != null && !(shape instanceof DraggableImage)) {
            Color fillColor = (Color) shape.getFill();
            Color strokeColor = (Color) shape.getStroke();
            double lineThickness = shape.getStrokeWidth();

        }
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    public void initStyle() {
        // NOTE THAT EACH CLASS SHOULD CORRESPOND TO
        // A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
        // CSS FILE
        canvas.getStyleClass().add(CLASS_RENDER_CANVAS);

        // COLOR PICKER STYLE
        editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        row1VBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row2VBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row3HBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row4VBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row5VBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row6VBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        gui.updateToolbarControls(false);
    }

    /**
     * This function reloads all the controls for editing logos the workspace.
     */
    @Override
    public void reloadWorkspace(AppDataComponent data) {
        app.getGUI().updateToolbarControls(false);
    }

    /**
     * This method will handle all the code pertaining to the implementation of
     * two different languages.
     */
    public void handleChangeLanguage(String propertiesFile) {
        if (propertiesFile.equals(ENGLISH)) {
            //System.out.print(gui.getTopToolbarPane().getChildren().get(8).accessibleHelpProperty().get());
        } else {

        }
    }

    @Override
    public void resetWorkspace() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Shape getTopShapeCanvas(double x, double y) {
        Shape metroO = null;
        for (int i = 0; i < canvas.getChildren().size(); i++) {
            if (canvas.getChildren().get(i).contains(x, y)) {
                if (canvas.getChildren().get(i) instanceof Text) {
                    metroO = (Text) canvas.getChildren().get(i);
                }
                if (canvas.getChildren().get(i) instanceof MetroLine) {
                    metroO = (MetroLine) canvas.getChildren().get(i);
                }
            }
        }
        return metroO;
    }
}
