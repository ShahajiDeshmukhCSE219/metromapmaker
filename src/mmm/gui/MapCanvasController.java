/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import mmm.data.AddImageAndTextController;
import mmm.data.DraggableImage;
import mmm.data.MetroLabel;
import mmm.data.MetroLine;
import mmm.data.MetroStation;
import mmm.data.mmmData;
import mmm.data.mmmState;

/**
 *
 * @author Shahaji
 */
public class MapCanvasController {
    //This is the app
    AppTemplate app;
    mmmData dataManager;

    public MapCanvasController(AppTemplate app) {
        this.app = app;
        dataManager=(mmmData)app.getDataComponent();
    }
    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    
    
    
    public void processMapEditorMouseDragged(double x, double y){
        mmmWorkspace work=(mmmWorkspace)app.getWorkspaceComponent();
        if(dataManager.isInState(mmmState.DRAGGING_METRO_SHAPE)){
            if((dataManager.getTopMetroObject(x, y) instanceof MetroLine)){
                MetroLine ml=(MetroLine)dataManager.getTopMetroObject(x, y);
                double xDiff=ml.getEndX()-ml.getStartX();
                double yDiff=ml.getEndY()-ml.getStartY();
                ml.startXProperty().unbind();ml.startYProperty().unbind();
                ml.setStartX(x);ml.setStartY(y);ml.setEndX(xDiff+x);ml.setEndY(yDiff+y);
                dataManager.setState(mmmState.NOTHING);
            }else if(dataManager.getTopMetroObject(x, y) instanceof MetroStation){
                MetroStation ms=(MetroStation)dataManager.getTopMetroObject(x, y);
                ms.setCenterX(x);ms.setCenterY(y);
            }else if(dataManager.getTopMetroObject(x, y) instanceof MetroLabel){
                System.out.println("--------in dragging label----------");
                MetroLabel mLabel=(MetroLabel)dataManager.getTopMetroObject(x, y);
                mLabel.setX(x);mLabel.setY(y);
            }else if(dataManager.getTopMetroObject(x, y) instanceof DraggableImage){
                DraggableImage di=(DraggableImage)dataManager.getTopMetroObject(x, y);
                di.setX(x);di.setY(y);
            }
        }else if(dataManager.isInState(mmmState.DRAGGING_METRO_LINE_END)){
            Text t=(Text)work.getTopShapeCanvas(x, y);
            MetroLine l=(MetroLine)dataManager.findMetroObject(t.getText(), "line");
            System.out.println("Check for line end drag-----"+l);
            t.xProperty().unbind();t.yProperty().unbind();
            l.startXProperty().bind(t.xProperty());
            l.startYProperty().bind(t.yProperty());
            t.setX(x);t.setY(y);
            
        
        app.getGUI().getTopToolbarPane().setOnMouseClicked(e->{
                app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
                l.startXProperty().unbind();l.startYProperty().unbind();
                dataManager.setState(mmmState.NOTHING);
                });
                work.editToolbar.setOnMouseClicked(e->{
                app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
                dataManager.setState(mmmState.NOTHING);
                l.startXProperty().unbind();l.startYProperty().unbind();
                });
        }
    }
    /**
     * Respond to mouse presses on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processMapEditorMousePress(double x, double y){
        mmmWorkspace work=(mmmWorkspace)app.getWorkspaceComponent();
        
        if(dataManager.isInState(mmmState.ADD_STATION_MODE)){
        DataDialogSingleton dds=new DataDialogSingleton("");
        String name=dds.showdialog();
        MetroStation ms=dataManager.startNewMetroStation(name);
        ms.setCenterX(x);
        ms.setCenterY(y);
        work.stationsCombobox.getItems().add(name);
        work.stationsCombobox.setValue(name);
        work.currentStation.getItems().add(name);
        work.destination.getItems().add(name);
        System.out.println(dataManager.getMetroMapShapes().toString());
        work.canvas.getChildren().addAll(ms,ms.getStationName());
        app.getGUI().getTopToolbarPane().setOnMouseClicked(e->{
                app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
                dataManager.setState(mmmState.NOTHING);
                });
                work.editToolbar.setOnMouseClicked(e->{
                app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
                dataManager.setState(mmmState.NOTHING);
                });
                System.out.print("the comboboex are here"+work.stationsCombobox.getItems());
        }else if(dataManager.isInState(mmmState.REMOVE_STATION_MODE)){
            if(dataManager.getTopMetroObject(x, y) instanceof MetroStation){
                //if(new DataDialogSingleton("bool").showYesNoDialog()){
                MetroStation ms=(MetroStation)dataManager.getTopMetroObject(x, y);
                MetroLine ml;
                    //ml=(MetroLine)ms.getLinesList().get(i);
                    ms.centerXProperty().unbind();
                    ms.centerYProperty().unbind();
                    
                work.stationsCombobox.getItems().remove(ms.getStationName().getText());
                dataManager.removeMetroObject(ms);
                work.canvas.getChildren().removeAll(ms, ms.getStationName());
                app.getGUI().getTopToolbarPane().setOnMouseClicked(e->{
                app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
                dataManager.setState(mmmState.NOTHING);
                });
                work.editToolbar.setOnMouseClicked(e->{
                app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
                dataManager.setState(mmmState.NOTHING);
                });
        }
                //}
            }else if(dataManager.isInState(mmmState.ADD_LINE_MODE)){
                MetroLine ml=dataManager.getCurrentLine();
                ml.setStartX(x);ml.setStartY(y);ml.setEndX(x+100);ml.setEndY(y);
                ml.bindText();
                work.canvas.getChildren().addAll(ml, ml.getLineName());
                dataManager.setState(mmmState.NOTHING);
            }else if(dataManager.isInState(mmmState.ADD_METRO_LABEL)){
                DataDialogSingleton dds=new DataDialogSingleton("Text");
                MetroLabel mLabel=dataManager.startNewMetroLabel(dds.showdialog());
                mLabel.setX(x);mLabel.setY(y);
                work.canvas.getChildren().add(mLabel);
                System.out.println(dataManager.getMetroMapShapes());
            } else if(dataManager.isInState(mmmState.ADD_METRO_IMAGE)){
                AddImageAndTextController ad=new AddImageAndTextController(app);
                DraggableImage di=ad.processAddImage();
                di.setX(x);di.setY(y);
                work.canvas.getChildren().add(di);
                dataManager.addMetroObject(di);
            } else if(dataManager.isInState(mmmState.NOTHING)){
                Shape cs=dataManager.getTopMetroObject(x, y);
                dataManager.setCurrentShape(cs);
                if(cs instanceof MetroStation){
                    MetroStation ms=(MetroStation)cs;
                    ((mmmWorkspace)app.getWorkspaceComponent()).stationsCombobox.setValue(ms.getStationName().getText());
                }else if(cs instanceof MetroLine){
                    MetroLine ml=(MetroLine)cs;
                    ((mmmWorkspace)app.getWorkspaceComponent()).metroLines.setValue(ml.getLineName().getText());
                }
            }
            app.getGUI().getTopToolbarPane().setOnMouseClicked(e->{
                app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
                dataManager.setState(mmmState.NOTHING);
                });
                work.editToolbar.setOnMouseClicked(e->{
                app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
                dataManager.setState(mmmState.NOTHING);
                });
        }
    
    /**
     * Respond to mouse button release on the rendering surface, which we call
     * canvas, but is actually a Pane.
     */
    public void processMapEditMouseRealese(int x, int y){
    }
}
