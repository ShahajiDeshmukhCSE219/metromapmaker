/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

/**
 * This enum has the various possible states of the MetroMapMaker app
 * during the editing process which helps us determine which controls
 * are usable or not and what specific user actions should affect.
 * @author Shahaji
 */
public enum mmmState {
    ADD_STATION_MODE,
    REMOVE_STATION_MODE,
    
    ADD_LINE_MODE, 
    REMOVE_LINE_MODE,
    
    ADD_METRO_LABEL,
    ADD_METRO_IMAGE,
    
    DRAGGING_METRO_SHAPE,
    DRAGGING_METRO_LINE_END,
    
    NOTHING
}
