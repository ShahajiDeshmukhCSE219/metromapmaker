/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

/**
 * This represents the MetroStation object on the map this is a part of the
 * Draggable family
 *
 * @author Shahaji
 */
public class MetroStation extends Circle implements Draggable {

    //The name of the station given by the user
    private Text stationName;
    //The list of the lines of which this station is part of. 
    private ObservableList<Node> linesList;

    private int TEMPCOUNTER;

    /**
     * Creates a MetroStation object with the given name
     *
     * @param stationName Creates a MetroStation object with the given name
     */
    public MetroStation(Text stationName) {
        this.stationName = stationName;
        setRadius(15);
        this.stationName.xProperty().bind(centerXProperty());
        this.stationName.yProperty().bind(centerYProperty().subtract(20));
        this.linesList = FXCollections.observableArrayList();
        TEMPCOUNTER = 0;
    }

    public Text getStationName() {
        return stationName;
    }

    /**
     * Creates a MetroStation object with the given name
     *
     * @param stationName Creates a MetroStation object with the given name
     * @param stationList Creates a MetroStation object with the given
     * stationsList
     */
    public MetroStation(Text stationName, ObservableList<Node> stationList) {
        this.stationName = stationName;
        this.linesList = stationList;
    }

    /**
     * Adds a MetroLine to the MetroStation's stationList
     *
     * @param line the MetroLine to be added.
     */
    public void addMetroLine(MetroLine line) {
        linesList.add(line);
    }

    /**
     * Removes a MetroLine to the MetroStation's stationList
     *
     * @param linethe MetroLine to be removed.
     * @return
     */
    public void removeMetroLine(MetroLine line) {
        this.linesList.remove(line);
    }

    public void moveLabel() {
        if (TEMPCOUNTER == 0) {
            this.stationName.xProperty().bind(centerXProperty());
            this.stationName.yProperty().bind(centerYProperty().subtract(20));
            TEMPCOUNTER++;
        } else if (TEMPCOUNTER == 1) {
            this.stationName.xProperty().bind(centerXProperty().add(20));
            this.stationName.yProperty().bind(centerYProperty().subtract(20));
            TEMPCOUNTER++;
        } else if (TEMPCOUNTER == 2) {
            this.stationName.xProperty().bind(centerXProperty().subtract(20));
            this.stationName.yProperty().bind(centerYProperty().subtract(20));
            TEMPCOUNTER++;
        } else if (TEMPCOUNTER == 3) {
            this.stationName.xProperty().bind(centerXProperty());
            this.stationName.yProperty().bind(centerYProperty().add(20));
            TEMPCOUNTER++;
        } else {
            TEMPCOUNTER = 0;
        }
    }

    /**
     * This checks whether the given MetroStation is on the MetroLine
     *
     * @param line to check if the given station is a pat of the MetroLine
     * @return boolean value of the result obtained.
     */
    public boolean containsMetroLine(MetroLine line) {
        return true;
    }

    @Override
    public mmmState getStartingState() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void start(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void drag(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void size(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getX() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getY() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getWidth() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getHeight() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShapeType() {
        return "MetroStation";
    }

    public void bind(Text name) {

    }

    public ObservableList<Node> getLinesList() {
        return linesList;
    }

}
