/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import static java.nio.file.Files.list;
import static java.util.Collections.list;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import mmm.gui.mmmWorkspace;

/**
 * Represents the MetroLine which can be added by the user part of the Draggable
 * family.
 *
 * @author Shahaji
 */
public class MetroLine extends Line implements Draggable {

    //The color of the line as chosen by the suer
    private Color lineColor;
    private FontWeight fw;
    private FontPosture fp;
    ObservableList<Line> connections;

    public MetroLine() {
    }

    public Color getLineColor() {
        return lineColor;
    }
    //The name of the line as chosen by the user
    private Text lineName;
    
    Line lastLine;
    
    private Text lineName2;

    public Text getLineName2() {
        return lineName2;
    }
    //The list of stations that make up the stationsList
    private ObservableList<Node> stationsList;

    /**
     * Creates a MetroLine object with the given line color and the line name
     *
     * @param lineColor the color of the line as chosen by the user
     * @param lineName the name of the line as chosen by the user
     * @pre lineColor and lineName should not be null
     */
    public MetroLine(Color lineColor, Text lineName) {
        this.lineColor = lineColor;
        this.lineName = lineName;
        setStrokeWidth(10);
        setStroke(lineColor);
        stationsList = FXCollections.observableArrayList();
        connections = FXCollections.observableArrayList();
    }

    public Text getLineName() {
        return lineName;
    }

    /**
     * Adds the given MetroStation to current MetroLine
     *
     * @param station the MetroStation object to be added
     * @pre station should not be null
     */
    public void addMetroStation(MetroStation station) {
        stationsList.add(station);
    }

    /**
     * Removes the given MetroStation to current MetroLine
     *
     * @param station the MetroStation object to be removed
     * @return the station which was removed from the MetroLine
     * @pre station should not be null
     */
    public MetroStation removeMetroStation(MetroStation station) {
        stationsList.remove(station);
        return station;
    }

    /**
     * This checks whether the given MetroStation is on the MetroLine
     *
     * @param station to check if the given station is a pat of the MetroLine
     * @return boolean value of the result obtained.
     */
    public boolean containsMetroStation(MetroStation station) {
        return stationsList.contains(station);
    }

    public void bindText() {
        //startXProperty().bindBidirectional(lineName.xProperty());
        //startYProperty().bindBidirectional(lineName.yProperty());
        lineName.xProperty().bind(startXProperty().subtract(20));
        lineName.yProperty().bind(startYProperty().subtract(25));
    }

    public ObservableList<Line> getConnections() {
        return this.connections;
    }

    public boolean isCircular() {
        boolean result = false;
        for (int i = 0; i < stationsList.size(); i++) {
            for (int j = i + 1; j < stationsList.size(); j++) {
                if (((MetroStation) stationsList.get(i)).getStationName().getText().equals(((MetroStation) stationsList.get(j)).getStationName().getText())) {
                    result = true;
                }
            }
        }
        return result;
    }

    public void setUpConnections(mmmData dm) {

        for (int i = 0; i < stationsList.size(); i++) {
            MetroStation ms = (MetroStation) stationsList.get(i);
            if (i == 0) {
                this.endXProperty().bindBidirectional(ms.centerXProperty());
                this.endYProperty().bindBidirectional(ms.centerYProperty());

            } else {
                Line l = new Line();
                l.setStrokeWidth(getStrokeWidth());
                l.setStroke(lineColor);
                MetroStation ms1 = (MetroStation) stationsList.get(i - 1);
                MetroStation ms2 = (MetroStation) stationsList.get(i);
                System.out.println("bind the start of " + ms1.getStationName().getText());
                System.out.println("bind the end of " + ms2.getStationName().getText());
                l.startXProperty().bind(ms1.centerXProperty());
                l.startYProperty().bind(ms1.centerYProperty());
                l.endXProperty().bind(ms.centerXProperty());
                l.endYProperty().bind(ms.centerYProperty());
                connections.add(l);
                lastLine=l;
                ((mmmWorkspace) dm.getApp().getWorkspaceComponent()).getCanvas().getChildren().add(l);

            }
        }
        //((mmmWorkspace)dm.getApp().getWorkspaceComponent()).getCanvas().getChildren().addAll(connections);
        connections.add(this);
        //System.out.println("this was the last value");
    }

    public Line setUpLine(MetroStation ms) {
        Line l = new Line();
        l.setStrokeWidth(getStrokeWidth());
        l.setStroke(lineColor);
        Text t=new Text("Hello");
        t.xProperty().bind(ms.centerXProperty());
        t.yProperty().bind(ms.centerYProperty());
        //Line l2=connections.get(connections.size()-1);
        l.startXProperty().bind(lastLine.endXProperty());
        l.startYProperty().bind(lastLine.endYProperty());
        l.endXProperty().bind(ms.centerXProperty());
        l.endYProperty().bind(ms.centerYProperty());
        ObservableList obs=FXCollections.observableArrayList();
        lastLine=l;
        return l;
    }

    public Line getLastLine() {
        return lastLine;
    }

    public void setLastLine(Line lastLine) {
        this.lastLine = lastLine;
    }

    @Override
    public mmmState getStartingState() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void start(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void drag(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void size(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getShapeType() {
        return "MetroLine";
    }

    public ObservableList<Node> getStationsList() {
        return stationsList;
    }

    @Override
    public double getX() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getY() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getWidth() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getHeight() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
