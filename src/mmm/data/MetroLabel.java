/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * This class represents the text object which can 
 * can be added by the user and belongs to the Draggable family. 
 * @author Shahaji
 */
public class MetroLabel extends Text {
    //Represents the text added by the user
    private String label;
    
    private FontPosture posture;
    private FontWeight weight;
    private double fontSize;

    public FontPosture getPosture() {
        return posture;
    }

    public void setPosture(FontPosture posture) {
        this.posture = posture;
    }

    public FontWeight getWeight() {
        return weight;
    }

    public void setWeight(FontWeight weight) {
        this.weight = weight;
    }

    public double getFontSize() {
        return fontSize;
    }

    public void setFontSize(double fontSize) {
        this.fontSize = fontSize;
        setFont(Font.font(this.fontSize));
    }
    /**
     * Creates a MetroLabel object with Text given by the user. 
     * @param label text which entered by the user
     * @pre label should not be null
     */
    public MetroLabel(String label) {
        setText(label);
        setFont(Font.font(25));
        posture=FontPosture.REGULAR;
        weight=FontWeight.NORMAL;
        fontSize=25;
        
    }
    
    public String getShapeType(){
        return "MetroLabel";
    }
    
    
}
