/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Shahaji
 */
public class DraggableImage extends Rectangle implements Draggable {
    double startX, startY;
    private File imageFile;
    Image img;
    public DraggableImage(){}
    
    public File getImageFile(){
        return this.imageFile;
    }
    public void setImageFile(File file){
        this.imageFile=file;
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
        setFill(new ImagePattern(img));
    }
    
    
    
    @Override
    public mmmState getStartingState() {
    return null;
    }

    @Override
    public void start(int x, int y) {
        startX = x;
        startY = y;
        setX(x);
        setY(y);
    }

    @Override
    public void size(int x, int y) {
        double width = x - getX();
        widthProperty().set(width);
        double height = y - getY();
        heightProperty().set(height);
    }

    @Override
    public void drag(int x, int y) {
         double diffX = x - startX;
        double diffY = y - startY;
        double newX = getX() + diffX;
        double newY = getY() + diffY;
        xProperty().set(newX);
        yProperty().set(newY);
        startX = x;
        startY = y;
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        startX=initX;
        startY=initY;
        setX(startX);
        setY(startY);
        setWidth(initWidth);
        setHeight(initHeight);
    }

    @Override
    public String getShapeType() {
        return "DraggableImage";
    }

    

    public double getStartX() {
        return startX;
    }

    public double getStartY() {
        return startY;
    }
    
    public double getwidth(){
    return getWidth();
    }
    
    public double getheight(){
    return getHeight();
    }
  
   
}