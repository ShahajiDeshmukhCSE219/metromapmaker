/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import com.sun.istack.internal.logging.Logger;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import javafx.scene.shape.Shape;
import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import mmm.gui.DataDialogSingleton;
import mmm.gui.mmmWorkspace;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;

/**
 * This class serves as the data management component for this application.
 *
 * @author Shahaji
 */
public class mmmData implements AppDataComponent {

    //This is the app
    private AppTemplate app;
    //this list contains all the object present in the map
    private ObservableList<Node> metroMapShapes;
    //The background image of the map
    private String backgroundImage;
    //the current state of the app
    private MetroLine currentLine;
    
    private String mapName;

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }
    
    public String getBackgroundImage() {
        return backgroundImage;
    }

    public AppTemplate getApp() {
        return app;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
        ((mmmWorkspace)app.getWorkspaceComponent()).getCanvas().setStyle("-fx-background-image: url(\""+this.backgroundImage+"\");"
                    + "-fx-background-size : 741px 1800px;");
    }

    public MetroLine getCurrentLine() {
        return currentLine;
    }

    public void setCurrentLine(MetroLine currentLine) {
        this.currentLine = currentLine;
    }
    private Color backgroundColor = Color.ALICEBLUE;

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
        String code = String.format("#%02X%02X%02X",
                    (int) (this.backgroundColor.getRed() * 255),
                    (int) (this.backgroundColor.getGreen() * 255),
                    (int) (this.backgroundColor.getBlue() * 255));
        ((mmmWorkspace)app.getWorkspaceComponent()).getCanvas().setStyle("-fx-background-color:" + code + ";");
    }

    private mmmState state;
    DropShadow dropShadow;

    private Shape currentShape;

    public mmmData(AppTemplate app) {
        this.app = app;
        dropShadow = new DropShadow();
        dropShadow.setRadius(5.0);
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.color(0.4, 0.5, 0.5));
        state=mmmState.NOTHING;
//        metroMapShapes=new ObservableList<Node>
    }

    /**
     * This adds the MetroObject to the map
     *
     * @param metroObject the shape to be added to the map
     */
    public void addMetroObject(Shape metroObject) {
        metroMapShapes.add(metroObject);
    }

    /**
     * This removes the MetroObject to the map
     *
     * @param metroObject the shape to be removed from the map
     * @return the shape which was removed from the map
     */
    public Shape removeMetroObject(Shape metroObject) {
        if (metroObject instanceof MetroLine) {
            MetroLine ml = (MetroLine) metroObject;
            for (int i = 0; i < ml.getStationsList().size(); i++) {
                ((MetroStation) ml.getStationsList().get(i)).centerXProperty().unbind();
                ((MetroStation) ml.getStationsList().get(i)).centerYProperty().unbind();
            }
        }
        metroMapShapes.remove(metroObject);
        return metroObject;
    }

    /**
     * Begins process of creating a new MetroLine object and adding it to the
     * the MetroMap
     *
     * @param color the color of the MetroLine object
     * @param name the name of the MetroLine object
     */
    public MetroLine startNewMetroLine(Color color, String name) {
        MetroLine line = new MetroLine(color, new Text(name));
        addMetroObject(line);
        return line;
    }

    /**
     * Begins process of creating a new MetroStation object and adding it to the
     * the MetroMap
     *
     * @param name the name of the MetroStation object
     */
    public MetroStation startNewMetroStation(String name) {
        MetroStation station = new MetroStation(new Text(name));
        addMetroObject(station);
        return station;
    }

    /**
     * Begins process of creating a new MetroLabel object and adding it to the
     * the MetroMap
     *
     * @param text the text of the MetroLabel object
     */
    public MetroLabel startNewMetroLabel(String text) {
        MetroLabel label = new MetroLabel(text);
        addMetroObject(label);
        return label;
    }

    /**
     * Begins process of creating a new DraggableImage object and adding it to
     * the the MetroMap
     *
     * @param image the image of the DraggableImage object
     */
    public void startNewDraggableImage() {
        AddImageAndTextController ad = new AddImageAndTextController(app);
        //DraggableImage di=new DraggableImage(ad.processAddImage());
        return;
    }

    /**
     * Returns the MetroObject which is at the top of the the given location.
     *
     * @return the shape which is that the position
     */
    public Shape getTopMetroObject(double x, double y) {
        Shape metroO = null;
        for (int i = 0; i < metroMapShapes.size(); i++) {
            if (metroMapShapes.get(i).contains(x, y)) {
                metroO = (Shape) metroMapShapes.get(i);
            }
        }
        return metroO;
    }

    /**
     * The MetroOobject which is at the bottom of another MetroObject can found
     * using this.
     *
     * @param metroObject the MetroObject below which there exists an
     * MetroObject
     * @return
     */
    public Shape getBottomMetroObject(Shape metroObject) {
        return null;
    }

    /**
     * Calculates the MetroStations ahead of a given MetroStation
     *
     * @param station the MetroStation in consideration.
     */
    public void stationsAhead(MetroStation station) {

    }

    /**
     * Calculates the MetroStations behind of a given MetroStation
     *
     * @param station the MetroStation in consideration.
     */
    public void stationsBehind(MetroStation station) {

    }

    /**
     * This method resets the data in Map
     */
    @Override
    public void resetData() {
        ((mmmWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().clear();; //To change body of generated methods, choose Tools | Templates.
    }

    public void setShapes(ObservableList<Node> metroShapes) {
        metroMapShapes = metroShapes;
    }

    public ObservableList<Node> getMetroMapShapes() {
        return metroMapShapes;
    }

    public Shape findMetroObject(String metroObject, String searchCase) {
        int location = -1;
        MetroLine ml;
        MetroStation ms;
        switch (searchCase) {
            case "line":
                for (int i = 0; i < metroMapShapes.size(); i++) {
                    if (metroMapShapes.get(i) instanceof MetroLine) {
                        ml = (MetroLine) metroMapShapes.get(i);
                        System.out.println();
                        if (ml.getLineName().getText().equals(metroObject)) {
                            location = i;
                        }
                    }
                }
                break;
            case "station":
                for (int i = 0; i < metroMapShapes.size(); i++) {
                    if (metroMapShapes.get(i) instanceof MetroStation) {
                        ms = (MetroStation) metroMapShapes.get(i);
                        System.out.println();
                        if (ms.getStationName().getText().equals(metroObject)) {
                            location = i;
                        }
                    }
                }
                break;
        }
        return (Shape) metroMapShapes.get(location);
    }

    public mmmState getState() {
        return state;
    }

    public void setState(mmmState state) {
        this.state = state;
    }

    public boolean isInState(mmmState state) {
        return this.state.equals(state);
    }

    public void addstationToLine(String lineName, MetroStation station) {
        MetroLine ml = (MetroLine) findMetroObject(lineName, "line");
        ml.addMetroStation(station);
    }

    public void addLineToStation(String name, MetroLine ml) {
        MetroStation ms = (MetroStation) findMetroObject(name, "station");
        ms.addMetroLine(ml);
    }

    public void setCurrentShape(Shape currentShape) {
        if(this.currentShape!=null){
        this.currentShape.setEffect(null);
        }
        this.currentShape = currentShape;
        this.currentShape.setEffect(dropShadow);
    }

    public Shape getCurrentShape() {
        return currentShape;
    }

    public void addMetroObjectFromLoad(Shape shape) {
        if(shape instanceof MetroStation){
        System.out.println("--------adding--------"+((MetroStation) shape).getStationName().getText());
        }
        addMetroObject(shape);
        app.getGUI().updateToolbarControls(false);
        ((mmmWorkspace)app.getWorkspaceComponent()).getCanvas().getChildren().add(shape);
        if(shape instanceof MetroLine){
            MetroLine ml=(MetroLine)shape;
            ((mmmWorkspace)app.getWorkspaceComponent()).getCanvas().getChildren().add((ml.getLineName()));
            ((mmmWorkspace)app.getWorkspaceComponent()).getMetroLines().getItems().add(ml.getLineName().getText());
        }else if(shape instanceof MetroStation){
            MetroStation ms=(MetroStation)shape;
            ((mmmWorkspace)app.getWorkspaceComponent()).getCanvas().getChildren().add((ms.getStationName()));
            ((mmmWorkspace)app.getWorkspaceComponent()).getStationsCombobox().getItems().add(ms.getStationName().getText());
        }
        ((mmmWorkspace)app.getWorkspaceComponent()).getCurrentStation().setItems(((mmmWorkspace)app.getWorkspaceComponent()).getStationsCombobox().getItems());
        ((mmmWorkspace)app.getWorkspaceComponent()).getDestination().setItems(((mmmWorkspace)app.getWorkspaceComponent()).getStationsCombobox().getItems());
    }
    
    public ObservableList<Node> seperateMetroLines(){
        ObservableList<Node> metroLines=FXCollections.observableArrayList();
        for(Node n : metroMapShapes){
            if(n instanceof MetroLine){
                metroLines.add(n);
            }
        }
        metroMapShapes.removeAll(metroLines);
        metroMapShapes.addAll(metroLines);
        System.out.println("---solution to all our problems------"+metroMapShapes.toString());
        return metroLines;
    }

}
