package mmm.data;


import djf.AppTemplate;
import mmm.data.DraggableImage;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import jtps.jTPS;
import mmm.gui.mmmWorkspace;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class AddImageAndTextController {
    private AppTemplate app;
     private File imageFile;
     
    public File getImageFile() {
        return imageFile;
    }
    public AddImageAndTextController(AppTemplate initApp) {
        app = initApp;
    }

    public DraggableImage processAddImage() {
        // ASK THE USER TO SELECT AN IMAGE
        Image imageToAdd = promptForImage();
        if (imageToAdd != null) {
            System.out.println("you printing put the address ");
//            DraggableImage imageViewToAdd = new DraggableImage(imageToAdd);
//            PropertiesManager props = PropertiesManager.getPropertiesManager();
//            
            DraggableImage di=new DraggableImage();
            di.setWidth(imageToAdd.getWidth());di.setHeight(imageToAdd.getHeight());
            di.setImg(imageToAdd);
            di.setImageFile(imageFile);
            return di;
            // MAKE AND ADD THE TRANSACTION
        }
        return null;
    }    

   
    
    
    private Image promptForImage() {
        // SETUP THE FILE CHOOSER FOR PICKING IMAGES
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("./images/"));
        FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.BMP");
        FileChooser.ExtensionFilter extFilterGIF = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterBMP, extFilterGIF, extFilterJPG, extFilterPNG);
        fileChooser.setSelectedExtensionFilter(extFilterPNG);
        
        // OPEN THE DIALOG
        File file = fileChooser.showOpenDialog(null);
        imageFile=file;
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            return image;
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    public String promptForImage1() {
        // SETUP THE FILE CHOOSER FOR PICKING IMAGES
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("./images/"));
        FileChooser.ExtensionFilter extFilterBMP = new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.BMP");
        FileChooser.ExtensionFilter extFilterGIF = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterBMP, extFilterGIF, extFilterJPG, extFilterPNG);
        fileChooser.setSelectedExtensionFilter(extFilterPNG);
        
        // OPEN THE DIALOG
        File file = fileChooser.showOpenDialog(null);
        return processbackgroundImage(file.getPath());
    }
    
    private String processbackgroundImage(String str){
        StringBuilder str1=new StringBuilder(str);
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)=='\\'){
                System.out.println("replce it");
                str1.setCharAt(i, '/');
        }
        }
        return str1.toString();
    }
}