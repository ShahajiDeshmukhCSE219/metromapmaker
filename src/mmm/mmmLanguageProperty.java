package mmm;

/**
 * This class provides the properties that are needed to be loaded from
 * language-dependent XML files.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public enum mmmLanguageProperty {
    SELECTION_TOOL_ICON,
    SELECTION_TOOL_TOOLTIP,
    
    REMOVE_ICON,
    REMOVE_TOOLTIP,
    
    RECTANGLE_ICON,
    RECTANGLE_TOOLTIP,
    
    ELLIPSE_ICON,
    ELLIPSE_TOOLTIP,
    
    MOVE_TO_BACK_ICON,
    MOVE_TO_BACK_TOOLTIP,
    MOVE_TO_FRONT_ICON,
    MOVE_TO_FRONT_TOOLTIP,
    
    BACKGROUND_TOOLTIP,
    FILL_TOOLTIP,
    OUTLINE_TOOLTIP,
    
    SNAPSHOT_ICON,
    SNAPSHOT_TOOLTIP,
    
    ABOUT_ICON, 
    ABOUT_TOOLTIP,
    
    COPY_ICON, 
    COPY_TOOLTIP, 
    
    CUT_ICON, 
    CUT_TOOLTIP, 
    
    UNDO_ICON,
    UNDO_TOOLTIP,
    
    REDO_ICON,
    REDO_TOOLTIP,
    
    PASTE_ICON, 
    PASTE_TOOLTIP,
    CHANGELANGUAGE_ICON,
    CHANGELANGUAGE_TOOLTIP,
    ROTATE_ICON, 
    ROTATE_TOOLTIP, 
    ROUTE_ICON, 
    ROUTE_TOOLTIP, 
    ZOOM_IN_ICON, 
    ZOOM_IN_TOOLTIP,
    ZOOM_OUT_ICON, 
    ZOOM_OUT_TOOLTIP,
    EXPAND_ICON, 
    EXPAND_TOOLTIP, 
    CONTRACT_ICON, 
    CONTRACT_TOOLTIP, 
    LIST_ICON, 
    LIST_TOOLTIP
    
    
}