/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.app;

import djf.AppTemplate;
import jtps.jTPS;
import mmm.data.mmmData;
import mmm.file.mmmFile;
import mmm.gui.mmmWorkspace;

/**
 *
 * @author Shahaji
 * This class serves as the application class for our MetroMapMaker application. 
 * Note that much of its behavior is inherited from AppTemplate, as defined in
 * the Desktop Java Framework. This app starts by loading all the app-specific
 * messages like icon files and tooltips and other settings, then the full 
 * User Interface is loaded using those settings. Note that this is a 
 * JavaFX application.
 */
public class MetroMapMaker extends AppTemplate{
   jtps.jTPS transactions;
    /**
     * This hook method must initialize all three components in the
     * proper order ensuring proper dependencies are respected, meaning
     * all proper objects are already constructed when they are needed
     * for use, since some may need others for initialization.
     */
    @Override
    public void buildAppComponentsHook(){
         // CONSTRUCT ALL THREE COMPONENTS. NOTE THAT FOR THIS APP
        // THE WORKSPACE NEEDS THE DATA COMPONENT TO EXIST ALREADY
        // WHEN IT IS CONSTRUCTED, AND THE DATA COMPONENT NEEDS THE
        // FILE COMPONENT SO WE MUST BE CAREFUL OF THE ORDER
        //The transactions component is also a part of this. 
        fileComponent = new mmmFile();
        dataComponent = new mmmData(this);
        workspaceComponent = new mmmWorkspace(this);
        transactions = new jTPS();
    }
    
    /**
     * This is where program execution begins. Since this is a JavaFX app it
     * will simply call launch, which gets JavaFX rolling, resulting in sending
     * the properly initialized Stage (i.e. window) to the start method inherited
     * from AppTemplate, defined in the Desktop Java Framework.
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
