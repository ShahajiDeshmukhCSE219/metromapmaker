/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.file;

import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import mmm.data.DraggableImage;
import mmm.data.MetroLabel;
import mmm.data.MetroLine;
import mmm.data.MetroStation;
import mmm.data.mmmData;

/**
 *
 * @author Shahaji
 */
public class mmmFile implements AppFileComponent {

    //These are the attributes that need to stored in the json file
    static final String JSON_BG_COLOR = "background_color";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";
    static final String JSON_METRO_SHAPES = "metro_shapes";
    static final String JSON_METRO_STATION_LIST = "stations_list";
    static final String JSON_METRO_LINES_LIST = "lines_station";
    static final String JSON_TYPE = "type";
    static final String JSON_NAME = "name";
    static final String JSON_LINE_COLOR = "line_color";
    static final String JSON_OUTLINE_THICKNESS = "outline_thickness";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_START_X = "start_x";
    static final String JSON_START_Y = "start_y";
    static final String JSON_END_X = "end_x";
    static final String JSON_END_Y = "end_y";
    static final String JSON_HEIGHT = "height";
    static final String JSON_WIDTH = "width";
    static final String JSON_RADIUS = "radius";
    static final String JSON_TEXT = "text";
    static final String JSON_FONTFAMILY = "font_family";
    static final String JSON_FONTSIZE = "font_size";
    static final String JSON_ISITALIC = "italicized";
    static final String JSON_ISBOLD = "emboldended";
    static final String JSON_IMAGEPATH = "path";
    static final String JSON_BACKGROUND_IMAGEPATH = "background image path";
    static final String JSON_ROTATE = "rotatation";
    static final String JSON_TEXT_COLOR = "text_color";

    /**
     * loads a MetroObject from the given jsonMetroObject
     *
     * @param jsonMetroObject the json object which is needed to create
     * MetroObject
     * @return the MetroObject which saved using JSON
     */
    private Shape loadMetroShape(JsonObject jsonMetroObject) {
        return null;
    }

    /**
     * This method used to convert json data which is a double.
     *
     * @param json the json data that needs to converted
     * @return the double that was stored in the json format
     */
    private double getDataAsDouble(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigDecimalValue().doubleValue();
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJsonFile(String jsonFilePath) throws FileNotFoundException, IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that together draws the logo.
     *
     * @param data The data management component for this application.
     *
     * @param filePath Path (including file name/extension) to where to save the
     * data to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    public void saveData(AppDataComponent data, String filePath) throws FileNotFoundException {
        mmmData dataManager = (mmmData) data;
        dataManager.seperateMetroLines();
        Color bgColor = dataManager.getBackgroundColor();
        JsonObject bgColorJson = makeJsonColorObject(bgColor);

        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        ObservableList<Node> metroShapes = dataManager.getMetroMapShapes();
        JsonObject metroJsonObject;
        for (Node metroObject : metroShapes) {
            if (metroObject instanceof MetroLine) {
                MetroLine ml = (MetroLine) metroObject;
                String type = ml.getShapeType();
                String name = ml.getLineName().getText();
                double startX = ml.getStartX();
                double startY = ml.getStartY();
                double endX = ml.getEndX();
                double endY = ml.getEndY();
                double outlineThickness = ml.getStrokeWidth();
                JsonObject lineColor = makeJsonColorObject(ml.getLineColor());
                JsonObject lineTextColor;
                if (ml.getLineName().getStroke() != null) {
                    lineTextColor = makeJsonColorObject((Color) ml.getLineName().getStroke());
                } else {
                    lineTextColor = makeJsonColorObject(Color.BLACK);
                }
                String fontFamily = ml.getLineName().getFont().getFamily();
                double fontSize = ml.getLineName().getFont().getSize();
                JsonObject stations = createJsonListObject(ml.getStationsList(), "stations");
                metroJsonObject = Json.createObjectBuilder()
                        .add(JSON_TYPE, type)
                        .add(JSON_NAME, name)
                        .add(JSON_START_X, startX)
                        .add(JSON_START_Y, startY)
                        .add(JSON_END_X, endX)
                        .add(JSON_END_Y, endY)
                        .add(JSON_OUTLINE_THICKNESS, outlineThickness)
                        .add(JSON_LINE_COLOR, lineColor)
                        .add(JSON_TEXT_COLOR, lineTextColor)
                        .add(JSON_FONTFAMILY, fontFamily)
                        .add(JSON_FONTSIZE, fontSize)
                        .add(JSON_METRO_STATION_LIST, stations)
                        .build();
                arrayBuilder.add(metroJsonObject);
            } else if (metroObject instanceof MetroStation) {
                MetroStation ms = (MetroStation) metroObject;
                String type = ms.getShapeType();
                String name = ms.getStationName().getText();
                double centerX = ms.getCenterX();
                double centerY = ms.getCenterY();
                double radius = ms.getRadius();
                JsonObject lines = createJsonListObject(ms.getLinesList(), "lines");
                String fontFamily = ms.getStationName().getFont().getFamily();
                double fontSize = ms.getStationName().getFont().getSize();
                JsonObject stationTextColor;
                if (ms.getStationName().getStroke() != null) {
                    stationTextColor = makeJsonColorObject((Color) ms.getStationName().getStroke());
                } else {
                    stationTextColor = makeJsonColorObject(Color.BLACK);
                }
                metroJsonObject = Json.createObjectBuilder()
                        .add(JSON_TYPE, type)
                        .add(JSON_NAME, name)
                        .add(JSON_X, centerX)
                        .add(JSON_Y, centerY)
                        .add(JSON_RADIUS, radius)
                        .add(JSON_FONTFAMILY, fontFamily)
                        .add(JSON_FONTSIZE, fontSize)
                        .add(JSON_TEXT_COLOR, stationTextColor)
                        .add(JSON_METRO_LINES_LIST, lines)
                        .build();
                arrayBuilder.add(metroJsonObject);
            } else if (metroObject instanceof MetroLabel) {
                MetroLabel mLabel = (MetroLabel) metroObject;
                String type = mLabel.getShapeType();
                String name = mLabel.getText();
                double x = mLabel.getX();
                double y = mLabel.getY();
                String fontFamily = mLabel.getFont().getFamily();
                double fontSize = mLabel.getFont().getSize();
                JsonObject textColor;
                if (mLabel.getStroke() != null) {
                    textColor = makeJsonColorObject((Color) mLabel.getStroke());
                } else {
                    textColor = makeJsonColorObject(Color.BLACK);
                }
                metroJsonObject = Json.createObjectBuilder()
                        .add(JSON_TYPE, type)
                        .add(JSON_NAME, name)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_FONTFAMILY, fontFamily)
                        .add(JSON_FONTSIZE, fontSize)
                        .add(JSON_TEXT_COLOR, textColor)
                        .build();
                arrayBuilder.add(metroJsonObject);
            } else if (metroObject instanceof DraggableImage) {
                DraggableImage di = (DraggableImage) metroObject;
                String imgPath = di.getImageFile().getAbsolutePath();
                String type = di.getShapeType();
                double x = di.getX();
                double y = di.getY();
                double width = di.getWidth();
                double height = di.getHeight();
                metroJsonObject = Json.createObjectBuilder()
                        .add(JSON_TYPE, type)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_WIDTH, width)
                        .add(JSON_HEIGHT, height)
                        .add(JSON_IMAGEPATH, imgPath)
                        .build();
                arrayBuilder.add(metroJsonObject);
            }

        }
        JsonArray shapesArray = arrayBuilder.build();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_BG_COLOR, bgColorJson)
                .add(JSON_METRO_SHAPES, shapesArray)
                .build();

        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    /**
     * This method loads data from a JSON formatted file into the data
     * management component and then forces the updating of the workspace such
     * that the user may edit the data.
     *
     * @param data Data management component where we'll load the file into.
     *
     * @param filePath Path (including file name/extension) to where to load the
     * data from.
     *
     * @throws IOException Thrown should there be an error reading in data from
     * the file.
     */
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
        mmmData dataManager = (mmmData) data;
        dataManager.resetData();

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJsonFile(filePath);

        // LOAD THE BACKGROUND COLOR
        Color bgColor = loadColor(json, JSON_BG_COLOR);
        dataManager.setBackgroundColor(bgColor);

        // AND NOW LOAD ALL THE SHAPES
        JsonArray jsonShapeArray = json.getJsonArray(JSON_METRO_SHAPES);
        for (int i = 0; i < jsonShapeArray.size(); i++) {
            JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
            Shape shape = loadShape(jsonShape, dataManager);
            dataManager.addMetroObjectFromLoad(shape);
        }
        for (int i = 0; i < jsonShapeArray.size(); i++) {
            JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
            System.out.println(jsonShape.getString(JSON_TYPE) + "----------sdfdsfsd");
            if (jsonShape.getString(JSON_TYPE).equals("MetroLine")) {
//                System.out.println("pass the types in the data manager");
//                for (int j = 0; j < dataManager.getMetroMapShapes().size(); j++) {
//                    if (dataManager.getMetroMapShapes().get(j) instanceof MetroLine) {
//                        System.out.println("in the loop now hopefully");
//                        MetroLine ml = (MetroLine) dataManager.getMetroMapShapes().get(i);
//                        confirmData(ml, jsonShape.getJsonObject(JSON_METRO_STATION_LIST), dataManager);
//                    }
//                }
                MetroLine ml = (MetroLine) dataManager.findMetroObject(jsonShape.getString(JSON_NAME), "line");
                confirmData(ml, jsonShape.getJsonObject(JSON_METRO_STATION_LIST), dataManager);
            }
        }

    }

    /**
     * This method is provided to satisfy the compiler, but it is not used by
     * this application.
     */
    public void importData(AppDataComponent data, String filePath) {

    }

    /**
     * This method is provided to give functionality for the export feature of
     * the MetroMapMaker
     */
    public void exportData(AppDataComponent data, String filePath) throws FileNotFoundException {
        mmmData dm = (mmmData) data;

        JsonArrayBuilder arrayBuilderL = Json.createArrayBuilder();
        JsonArrayBuilder arrayBuilderS = Json.createArrayBuilder();

        ObservableList<Node> metroShapes = dm.getMetroMapShapes();
        JsonObject metroJsonObject;
        for (Node metroObject : metroShapes) {
            System.out.println(metroObject.toString());
            if (metroObject instanceof MetroLine) {
                MetroLine ml = (MetroLine) metroObject;
                String name = ml.getLineName().getText();
                JsonObject lineColor = makeJsonColorObject(ml.getLineColor());
                JsonObject lineTextColor;
                if (ml.getLineName().getStroke() != null) {
                    lineTextColor = makeJsonColorObject((Color) ml.getLineName().getStroke());
                } else {
                    lineTextColor = makeJsonColorObject(Color.BLACK);
                }
                String fontFamily = ml.getLineName().getFont().getFamily();
                double fontSize = ml.getLineName().getFont().getSize();
                metroJsonObject = Json.createObjectBuilder()
                        .add("name", name)
                        .add("circular", ml.isCircular())
                        .add("color", lineColor)
                        .add("station_names ",createJsonListObject2(ml.getStationsList(), "stations"))
                        .build();
                arrayBuilderL.add(metroJsonObject);
            }else if(metroObject instanceof MetroStation){
                 MetroStation ms = (MetroStation) metroObject;
                String type = ms.getShapeType();
                String name = ms.getStationName().getText();
                double centerX = ms.getCenterX();
                double centerY = ms.getCenterY();
                double radius = ms.getRadius();
                JsonObject lines = createJsonListObject(ms.getLinesList(), "lines");
                String fontFamily = ms.getStationName().getFont().getFamily();
                double fontSize = ms.getStationName().getFont().getSize();
                JsonObject stationTextColor;
                if (ms.getStationName().getStroke() != null) {
                    stationTextColor = makeJsonColorObject((Color) ms.getStationName().getStroke());
                } else {
                    stationTextColor = makeJsonColorObject(Color.BLACK);
                }
                metroJsonObject = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_X, centerX)
                        .add(JSON_Y, centerY)
                        .build();
                arrayBuilderS.add(metroJsonObject);
            }
        }
            JsonArray lines = arrayBuilderL.build();
            JsonArray stations = arrayBuilderS.build();
            JsonObject dataManagerJSO = Json.createObjectBuilder()
                    .add(JSON_NAME, "Blue Line")
                    .add("lines", lines)
                    .add("stations", stations)
                    .build();
            Map<String, Object> properties = new HashMap<>(1);
            properties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
            StringWriter sw = new StringWriter();
            JsonWriter jsonWriter = writerFactory.createWriter(sw);
            jsonWriter.writeObject(dataManagerJSO);
            jsonWriter.close();

            // INIT THE WRITER
            OutputStream os = new FileOutputStream(filePath);
            JsonWriter jsonFileWriter = Json.createWriter(os);
            jsonFileWriter.writeObject(dataManagerJSO);
            String prettyPrinted = sw.toString();
            PrintWriter pw = new PrintWriter(filePath);
            pw.write(prettyPrinted);
            pw.close();
        
    }

    private JsonObject makeJsonColorObject(Color color) {
        JsonObject colorJson = Json.createObjectBuilder()
                .add(JSON_RED, color.getRed())
                .add(JSON_GREEN, color.getGreen())
                .add(JSON_BLUE, color.getBlue())
                .add(JSON_ALPHA, color.getOpacity()).build();
        return colorJson;
    }

    public JsonObject createJsonListObject(ObservableList<Node> list, String listType) {
        MetroStation ms = null;
        MetroLine ml;
        JsonArrayBuilder ja = Json.createArrayBuilder();
        if (!list.isEmpty()) {
            for (Node n : list) {
                if (listType.equals("stations")) {
                    ms = (MetroStation) n;
                    ja.add(ms.getStationName().getText());
                } else if (listType.equals("lines")) {
                    ml = (MetroLine) n;
                    ja.add(ml.getLineName().getText());
                }

            }
            JsonObject jo = Json.createObjectBuilder().
                    add(listType, ja.build())
                    .build();
            return jo;
        } else {
            return Json.createObjectBuilder().build();
        }
    }

    public JsonArray createJsonListObject2(ObservableList<Node> list, String listType) {
        MetroStation ms = null;
        MetroLine ml;
        JsonArrayBuilder ja = Json.createArrayBuilder();
        if (!list.isEmpty()) {
            System.out.println("dsjfhsdfkjhsdf");
            for (Node n : list) {
                if (listType.equals("stations")) {
                    ms = (MetroStation) n;
                    ja.add(ms.getStationName().getText());
                    System.out.println(ms.getStationName().getText());
                }
            }
            
            return ja.build();
        } else {
            return Json.createArrayBuilder().build();
        }
    }

    private Color loadColor(JsonObject json, String colorToGet) {
        JsonObject jsonColor = json.getJsonObject(colorToGet);
        double red = getDataAsDouble(jsonColor, JSON_RED);
        double green = getDataAsDouble(jsonColor, JSON_GREEN);
        double blue = getDataAsDouble(jsonColor, JSON_BLUE);
        double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
        Color loadedColor = new Color(red, green, blue, alpha);
        return loadedColor;
    }

    private Shape loadShape(JsonObject jsonShape, mmmData dataManager) throws IOException {

        Shape shape = null;
        String type = jsonShape.getString(JSON_TYPE);
        if (type.equals("MetroLine")) {
            System.out.println(type);
            System.out.println(jsonShape.getString(JSON_NAME));
            Color c = loadColor(jsonShape, JSON_LINE_COLOR);
            MetroLine ml;
            ml = new MetroLine(loadColor(jsonShape, JSON_LINE_COLOR), new Text(jsonShape.getString(JSON_NAME)));
            ml.setStartX((jsonShape.getInt(JSON_START_X)));
            ml.setStartY((jsonShape.getInt(JSON_START_Y)));
            ml.setEndX((jsonShape.getInt(JSON_END_X)));
            ml.setEndY((jsonShape.getInt(JSON_END_Y)));
            ml.setStrokeWidth(jsonShape.getInt(JSON_OUTLINE_THICKNESS));
            ml.getLineName().setStroke(loadColor(jsonShape, JSON_TEXT_COLOR));
            ml.bindText();
            shape = ml;
        } else if (type.equals("MetroStation")) {
            MetroStation ms;
            ms = new MetroStation((new Text(jsonShape.getString(JSON_NAME))));
            ms.setRadius(jsonShape.getInt(JSON_RADIUS));
            ms.setCenterX(jsonShape.getInt(JSON_X));
            ms.setCenterY(jsonShape.getInt(JSON_Y));
            ms.getStationName().setFont(Font.font(jsonShape.getString(JSON_FONTFAMILY), jsonShape.getInt(JSON_FONTSIZE)));
            ms.getStationName().setStroke(loadColor(jsonShape, JSON_TEXT_COLOR));
            shape = ms;
        } else if (type.equals("MetroLabel")) {
            MetroLabel mLabel;
            mLabel = new MetroLabel(jsonShape.getString(JSON_NAME));
            mLabel.setX(jsonShape.getInt(JSON_X));
            mLabel.setY(jsonShape.getInt(JSON_Y));
            mLabel.setStroke(loadColor(jsonShape, JSON_TEXT_COLOR));
            mLabel.setFont(Font.font(jsonShape.getString(JSON_FONTFAMILY), jsonShape.getInt(JSON_FONTSIZE)));
            shape = mLabel;
        } else if (type.equals("DraggableImage")) {
            DraggableImage di;
            String path = jsonShape.getString(JSON_IMAGEPATH);
            di = new DraggableImage();
            di.setHeight(jsonShape.getInt(JSON_HEIGHT));
            di.setWidth(jsonShape.getInt(JSON_WIDTH));
            di.setX(jsonShape.getInt(JSON_X));
            di.setY(jsonShape.getInt(JSON_Y));
            File f = new File(path);
            BufferedImage bufferedImage = ImageIO.read(f);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            di.setImg(image);
            shape = di;
        }
        return shape;
    }

    private void confirmData(MetroLine ml, JsonObject jsonObj, mmmData dm) {
        System.out.println("in this---confirm data");
        System.out.println(jsonObj);
        if (!jsonObj.isEmpty()) {
            JsonArray ja = jsonObj.getJsonArray("stations");
            String stationName;

            for (int i = 0; i < ja.size(); i++) {
                System.out.println("---should be in file--------" + ja.size());
                stationName = ja.getString(i);
                MetroStation ms = (MetroStation) dm.findMetroObject(stationName, "station");
                ml.addMetroStation(ms);
                ms.addMetroLine(ml);
                ml.setUpConnections(dm);
            }
        }
    }
}
